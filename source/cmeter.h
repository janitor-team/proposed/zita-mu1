// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2015 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __CMETER_H
#define	__CMETER_H


#include <clxclient.h>


class Cmeter : public X_window
{
public:

    enum { HOR, VER };
    enum { LINEW = 6, LENGTH = 143 };

    Cmeter (X_window *parent, int xpos, int ypos, int geom);
    ~Cmeter (void);
    Cmeter (const Cmeter&);
    Cmeter& operator=(const Cmeter&);

    void update (float v);

    int xs (void) const { return _xs; }
    int ys (void) const { return _ys; }

    XImage *scale (void) const
    {
	return (_geom == HOR) ? _stc_scaleH : _stc_scaleV;
    }

    static void load_images (X_display *disp, const char *path);

    static XImage *_stc_scaleH;
    static XImage *_stc_scaleV;

private:

    int        _geom;
    int        _xs;
    int        _ys;
    int        _k;
    Pixmap     _pixm;
    XImage    *_imag0;
    XImage    *_imag1;

    static XImage *_stc_meterH0;
    static XImage *_stc_meterH1;
    static XImage *_stc_meterV0;
    static XImage *_stc_meterV1;
};


#endif
