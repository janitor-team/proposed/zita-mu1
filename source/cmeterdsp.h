// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __CMETERDSP_H
#define	__CMETERDSP_H


class Cmeterdsp
{
public:

    Cmeterdsp (void);
    ~Cmeterdsp (void);

    void  reset (void) {_zl = _zr = _zlr = _zll = _zrr = 0.0f; }
    void  process (float *pl, float *pr, int n);  
    float read (void);

    static void init (int fsamp, float flp, float tcf); 

private:

    float          _zl;          // filter states
    float          _zr;
    float          _zlr;
    float          _zll;
    float          _zrr;

    static float   _w1;          // lowpass filter coefficient
    static float   _w2;          // correlation filter coeffient
};


#endif
